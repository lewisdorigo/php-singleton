<?php namespace Dorigo\Singleton\Test\SingletonTest;

class SingletonTestClass extends \Dorigo\Singleton\Singleton {

    private $data;

    protected function __construct() {}

    public function getData() {
        return $this->data;
    }

    public function setData($data) {
        $this->data = $data;
    }
}