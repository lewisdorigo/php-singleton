<?php namespace Dorigo\Singleton\Test;

use PHPUnit\Framework\TestCase;
use Dorigo\Singleton\Singleton;
use Dorigo\Singleton\Test\SingletonTest\SingletonTestClass;

class SingletonTest extends TestCase {

    /**
     * @dataProvider dataReturnsValidInstance
     * @param string $singletonClassName
     */
    public function testReturnsValidInstance($className) {
        $instance = ($className)::getInstance();

        $this->assertEquals($className, get_class($instance));
    }

    public function dataReturnsValidInstance(){
        return [
            [SingletonTestClass::class],
        ];
    }



    /**
     * @dataProvider dataReturnsOnlyOneInstance
     * @param string $singletonClassName
     * @param Singleton $expectedSingletonInstance
     */
    public function testReturnsOnlyOneInstance($className, Singleton $expectedSingleton) {
        $instance = ($className)::getInstance();

        $this->assertSame($expectedSingleton, $instance);

        $expectedSingleton->setData(rand(0,9999));

        $this->assertEquals($expectedSingleton->getData(), $instance->getData());
    }

    public function dataReturnsOnlyOneInstance() {
        return [
            [SingletonTestClass::class, SingletonTestClass::getInstance()],
        ];
    }
}