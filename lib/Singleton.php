<?php namespace Dorigo\Singleton;

abstract class Singleton {
    protected static $instances = [];

    public static function getInstance() {
        $calledClass = get_called_class();

        if(!array_key_exists($calledClass, self::$instances)) {
            self::$instances[$calledClass] = new $calledClass();
        }

        return self::$instances[$calledClass];
    }

    abstract protected function __construct();
}