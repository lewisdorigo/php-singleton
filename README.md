# PHP Singleton

An abstract class for creating Singletons in PHP

## Usage of PHP Singleton

### Delcaring a Singleton
``` php
<?php
class MySingleton extends /Dorigo/Singleton/Singleton {
    private $data;
    
    protected function __construct() {}
    
    public function getData() {
        return $this->data;
    }
    
    public function setData($data) {
        $this->data = $data;
    }
}

```

### Using a Singleton
``` php
<?php

$a = MySingleton::getInstance();
$b = MySingleton::getInstance();

$a->setData("test");

if($b->getData() === "test") {
    echo "Data is the same!"
}
```